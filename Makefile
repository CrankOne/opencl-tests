     CC=gcc
 CFLAGS=-std=c99 -Wall #-DUNIX -g -DDEBUG
   LIBS=-l:libOpenCL.so.1

square: clbuild.o square.o
	g++ $^ -o $@ $(LIBS)

%.o : %.c
	$(CC) -c $^ $(CFLAGS) -o $@

clean:
	rm -f ./*.o
	rm -f square
